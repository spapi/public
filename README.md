# Profile of Ferenc Buzás #

I am a senior full stack developer in Hungary.

*Actual programming languages:*  Java 8 (SE 7: OCP exam), JavaScript, Python, C++
*Frontend:* JavaScript, Vaadin, JSF, Angular 2, HTML, CSS
*Backend:* WildFly, EJB, JPA / Spring; SQL DBs (Mysql, Oracle, ...)
*Scripting languages:* Python, bash
*GUI:* Swing (great experience with it)
*Others:* JUnit (Mockito), Selenium, Cucumber, CI (Jenkins, Hudson etc.)
*Favourite dev. environment/tools:*  Linux, IntelliJ IDEA, Java, Spring, Angular or Vaadin
*Hobbies:* playing the piano/organ, playing go, biking, swimming
## Details: ##
[CV in English](https://drive.google.com/open?id=1MdRGPxttlghfELGdehirmN5p_6iL7IG2)
[CV in Hungarian (önéletrajz)](https://drive.google.com/open?id=1c109JmPOW946lXl_NFsfAb6ZFkXBL9iu)
[LinkedIn profile](http://www.linkedin.com/in/ferenc-buzas)

